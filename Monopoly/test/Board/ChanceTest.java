package Board;

import Players.Player;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ChanceTest {
    static MonopolyBoard testBoard;
    static Space testSpace;
    static Player testPlayer;
    static Player otherPlayer;

    @BeforeClass
    public static void initialize() {
        testBoard = new MonopolyBoard();
        testSpace = new Chance(testBoard.go, testBoard.go.prev, "Chance");
        testPlayer = new Player("Test", 1, testSpace, testBoard);
        testPlayer.houseCount = 2;
        testPlayer.hotelCount = 1;
        otherPlayer = new Player("other", 2, testBoard.go, testBoard);
        testBoard.addPlayer(testPlayer);
        testBoard.addPlayer(otherPlayer);
    }

    @Before
    public void resetSpace() {
        testPlayer.setSpace(testSpace);
    }

    @Test
    public void testChance1() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }
    @Test
    public void testChance2() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }
    @Test
    public void testChance3() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }
    @Test
    public void testChance4() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }
    @Test
    public void testChance5() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }
    @Test
    public void testChance6() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }
    @Test
    public void testChance7() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }
    @Test
    public void testChance8() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }
    @Test
    public void testChance9() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }
    @Test
    public void testChance10() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }
    @Test
    public void testChance11() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }
    @Test
    public void testChance12() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }
    @Test
    public void testChance13() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }
    @Test
    public void testChance14() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }
    @Test
    public void testChance15() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }
    @Test
    public void testChance16() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }

}
