package Board;

import Players.Player;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotEquals;

public class GoTest {
    Player testPlayer;
    Player testPlayer2;
    MonopolyBoard testBoard;

    @Before
    public void init() {
        testBoard = new MonopolyBoard();
        testPlayer = new Player("Test", 1, testBoard.go, testBoard);
        testPlayer2 = new Player("Test2", 2, testBoard.go, testBoard);
        testBoard.addPlayer(testPlayer);
        testBoard.addPlayer(testPlayer2);
    }

    @Test
    public void testAction() {
        Player copyPlayer = new Player(testPlayer);
        System.out.println(testBoard.toString());
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }

}
