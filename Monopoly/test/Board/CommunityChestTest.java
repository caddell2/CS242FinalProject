package Board;

import Players.Player;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertNotEquals;

public class CommunityChestTest {

    static MonopolyBoard testBoard;
    static Space testSpace;
    static Player testPlayer;
    static Player otherPlayer;

    @BeforeClass
    public static void initialize() {
        testBoard = new MonopolyBoard();
        testSpace = new CommunityChest(testBoard.go, testBoard.go.prev, "Community Chest");
        testPlayer = new Player("Test", 1, testSpace, testBoard);
        testPlayer.houseCount = 2;
        testPlayer.hotelCount = 1;
        otherPlayer = new Player("other", 2, testBoard.go, testBoard);
        testBoard.addPlayer(testPlayer);
        testBoard.addPlayer(otherPlayer);
    }

    @Before
    public void init() {
        testPlayer.setSpace(testSpace);
    }

    @Test
    public void testCC1() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }

    @Test
    public void testCC2() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }

    @Test
    public void testCC3() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }

    @Test
    public void testCC4() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }

    @Test
    public void testCC5() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }

    @Test
    public void testCC6() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }

    @Test
    public void testCC7() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }

    @Test
    public void testCC8() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }

    @Test
    public void testCC9() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }

    @Test
    public void testCC10() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }

    @Test
    public void testCC11() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }

    @Test
    public void testCC12() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }

    @Test
    public void testCC13() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }

    @Test
    public void testCC14() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }

    @Test
    public void testCC15() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }

    @Test
    public void testCC16() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertNotEquals(copyPlayer, testPlayer);
    }

}
