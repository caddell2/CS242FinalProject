package Board;

import Players.Player;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class FreeParkingTest {
    Player testPlayer;
    Space testSpace;
    MonopolyBoard testBoard;

    @Before
    public void init() {
        testBoard = new MonopolyBoard();
        testSpace = new FreeParking(testBoard.go, testBoard.go.prev, "Free Parking");
        testPlayer = new Player("Test", 1, testSpace, testBoard);
    }

    @Test
    public void testAction() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        assertEquals(copyPlayer, testPlayer);
    }

}
