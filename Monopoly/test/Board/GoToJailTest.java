package Board;

import Players.Player;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class GoToJailTest {
    Player testPlayer;
    Space testSpace;
    MonopolyBoard testBoard;

    @Before
    public void init() {
        testBoard = new MonopolyBoard();
        testSpace = new GoToJail(testBoard.go, testBoard.go.prev, "Go to jail", testBoard.jail);
        testPlayer = new Player("Test", 1, testSpace, testBoard);
        testBoard.addPlayer(testPlayer);
    }

    @Test
    public void testAction() {
        Player copyPlayer = new Player(testPlayer);
        testPlayer.doSpace();
        System.out.println(testBoard.toString());
        assertNotEquals(copyPlayer, testPlayer);
    }

}
