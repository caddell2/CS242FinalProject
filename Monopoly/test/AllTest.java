import Board.*;
import org.junit.AfterClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ChanceTest.class, CommunityChestTest.class, FreeParkingTest.class, GoTest.class, GoToJailTest.class})

public class AllTest {
}
