package Pieces.CommunityChestCards;

import Players.Player;

public class BankError extends CommunityChestCard {

    public BankError() {
        this.description = "Bank error in your favor - Collect $200";
    }

    public void action(Player currPlayer) {
        currPlayer.chargeBank(200);
    }
}
