package Pieces.CommunityChestCards;

import Players.Player;

public class DoctorFees extends CommunityChestCard {

    public DoctorFees() {
        this.description = "Doctor's fees - Pay $50";
    }

    public void action(Player currPlayer) {
        currPlayer.giveMoneyToBank(50);
    }
}
