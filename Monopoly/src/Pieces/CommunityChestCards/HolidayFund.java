package Pieces.CommunityChestCards;

import Players.Player;

public class HolidayFund extends CommunityChestCard {

    public HolidayFund() {
        this.description = "Holiday Fund matures - Receive $100";
    }

    public void action(Player currPlayer) {
        currPlayer.chargeBank(100);
    }
}
