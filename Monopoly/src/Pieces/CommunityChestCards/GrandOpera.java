package Pieces.CommunityChestCards;

import Players.Player;

public class GrandOpera extends CommunityChestCard {

    public GrandOpera() {
        this.description = "Grand Opera Night – Collect $50 from every player for opening night seats";
    }

    public void action(Player currPlayer) {
        currPlayer.chargeAllOthers(50);
    }
}
