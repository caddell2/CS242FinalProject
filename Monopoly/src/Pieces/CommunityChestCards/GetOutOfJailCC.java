package Pieces.CommunityChestCards;

import Players.Player;

public class GetOutOfJailCC extends CommunityChestCard {

    public GetOutOfJailCC() {
        this.description = "Get Out of Jail Free – This card may be kept until needed or sold";
    }

    public void action(Player currPlayer) {
        currPlayer.hasCommunityGetOutofJailCard = true;
    }
}
