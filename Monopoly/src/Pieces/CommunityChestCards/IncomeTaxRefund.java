package Pieces.CommunityChestCards;

import Players.Player;

public class IncomeTaxRefund extends CommunityChestCard {

    public IncomeTaxRefund() {
        this.description = "Income tax refund – Collect $20";
    }

    public void action(Player currPlayer) {
        currPlayer.chargeBank(20);
    }
}
