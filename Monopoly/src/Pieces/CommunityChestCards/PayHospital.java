package Pieces.CommunityChestCards;

import Players.Player;

public class PayHospital extends CommunityChestCard {

    public PayHospital() {
        this.description = "Pay hospital fees of $100";
    }

    public void action(Player currPlayer) {
        currPlayer.giveMoneyToBank(100);
    }

}
