package Pieces.CommunityChestCards;

import Players.Player;

public class YouReceivedInheitance extends CommunityChestCard {

    public YouReceivedInheitance() {
        this.description = "You inherit $100";
    }

    public void action(Player currPlayer) {
        currPlayer.chargeBank(100);
    }

}
