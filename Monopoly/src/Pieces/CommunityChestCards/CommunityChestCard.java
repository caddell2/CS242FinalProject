package Pieces.CommunityChestCards;

import Players.Player;

public abstract class CommunityChestCard {

    String description;

    public String getDescrip() {
        return description;
    }

    public abstract void action(Player currPlayer);
}
