package Pieces.CommunityChestCards;

import Players.Player;

public class FromStockSale extends CommunityChestCard {

    public FromStockSale() {
        this.description = "From sale of stock you get $50";
    }

    public void action(Player currPlayer) {
        currPlayer.chargeBank(50);
    }
}
