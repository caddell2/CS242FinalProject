package Pieces.CommunityChestCards;

import Board.Space;
import Players.Player;

public class GoToJailCC extends CommunityChestCard {

    Space jail;

    public GoToJailCC(Space jail) {
        this.description = "Go directly to jail – Do not pass Go – Do not collect $200";
        this.jail = jail;
    }

    public void action(Player currPlayer) {
        currPlayer.setSpace(jail);
    }

}
