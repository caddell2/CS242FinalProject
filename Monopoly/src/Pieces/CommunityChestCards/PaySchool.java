package Pieces.CommunityChestCards;

import Players.Player;

public class PaySchool extends CommunityChestCard {

    public PaySchool() {
        this.description = "Pay school fees of $150";
    }

    public void action(Player currPlayer) {
        currPlayer.giveMoneyToBank(150);
    }

}
