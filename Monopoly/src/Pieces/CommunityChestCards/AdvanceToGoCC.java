package Pieces.CommunityChestCards;

import Board.Space;
import Players.Player;

public class AdvanceToGoCC extends CommunityChestCard {

    Space go;

    public AdvanceToGoCC(Space go) {
        this.description = "Advance to Go";
        this.go = go;
    }

    public void action(Player currPlayer) {
        currPlayer.setSpace(go);
    }

}
