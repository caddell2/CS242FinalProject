package Pieces.CommunityChestCards;

import Players.Player;

public class YouWonBeauty extends CommunityChestCard {

    public YouWonBeauty() {
        this.description = "You have won second prize in a beauty contest – Collect $10";
    }

    public void action(Player currPlayer) {
        currPlayer.chargeBank(10);
    }

}
