package Pieces.CommunityChestCards;

import Players.Player;

public class ReceiveConsultantFee extends CommunityChestCard {

    public ReceiveConsultantFee() {
        this.description = "Receive $25 consultancy fee";
    }

    public void action(Player currPlayer) {
        currPlayer.chargeBank(25);
    }

}
