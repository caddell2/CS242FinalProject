package Pieces.CommunityChestCards;

import Players.Player;

public class LifeInsurance extends CommunityChestCard {

    public LifeInsurance() {
        this.description = "Life insurance matures – Collect $100";
    }

    public void action(Player currPlayer) {
        currPlayer.chargeBank(100);
    }

}
