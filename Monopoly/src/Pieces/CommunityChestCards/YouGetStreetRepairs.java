package Pieces.CommunityChestCards;

import Players.Player;

public class YouGetStreetRepairs extends CommunityChestCard {

    public YouGetStreetRepairs() {
        this.description = "You are assessed for street repairs – $40 per house – $115 per hotel";
    }

    public void action(Player currPlayer) {
        currPlayer.chargePerUnit(40, 115);
    }

}
