package Pieces.ChanceCards;

import Players.Player;

public class BankPaysYou extends ChanceCard {

    public BankPaysYou() {
        this.description = "Bank pays you dividend of $50";
    }

    public void action(Player currPlayer) {
        currPlayer.chargeBank(50);
    }
}
