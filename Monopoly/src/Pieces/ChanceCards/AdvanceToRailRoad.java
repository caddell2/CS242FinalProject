package Pieces.ChanceCards;

import Board.Railroad;
import Board.Space;
import Players.Player;

public class AdvanceToRailRoad extends ChanceCard {

    public AdvanceToRailRoad() {
        this.description = "Advance token to the nearest Railroad and pay owner twice the rental to which he/she {he} "+
                "is otherwise entitled. If Railroad is unowned, you may buy it from the Bank.";
    }

    public Space findClosestRR(Space currSpace) {
        if(currSpace instanceof Railroad){
            return currSpace;
        }
        else {
            return findClosestRR(currSpace.move());
        }
    }

    public void action(Player currPlayer) {
        currPlayer.setSpace(findClosestRR(currPlayer.currSpace));
    }

}
