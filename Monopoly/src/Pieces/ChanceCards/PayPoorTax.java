package Pieces.ChanceCards;

import Players.Player;

public class PayPoorTax extends ChanceCard {

    public PayPoorTax() {
        this.description = "Pay poor tax of $15";
    }

    public void action(Player currPlayer) {
        currPlayer.giveMoneyToBank(15);
    }

}
