package Pieces.ChanceCards;

import Players.Player;

public class GetOutOfJailChance extends ChanceCard {

    public GetOutOfJailChance() {
        this.description = "Get out of Jail Free – This card may be kept until needed, or traded/sold";
    }

    public void action(Player currPlayer) {
        currPlayer.hasChanceGetOutofJailCard = true;
    }
}
