package Pieces.ChanceCards;

import Players.Player;

public class YouAreChairman extends ChanceCard {

    public YouAreChairman() {
        this.description = "You have been elected Chairman of the Board – Pay each player $50";
    }

    public void action(Player currPlayer) {
        currPlayer.payAllOthers(50);
    }
}
