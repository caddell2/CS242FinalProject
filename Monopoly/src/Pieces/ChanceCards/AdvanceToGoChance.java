package Pieces.ChanceCards;

import Board.Space;
import Players.Player;

public class AdvanceToGoChance extends ChanceCard {

    Space go;

    public AdvanceToGoChance(Space go) {
        this.description = "Advance to Go";
        this.go = go;
    }

    public void action(Player currPlayer) {
        currPlayer.setSpace(go);
    }

}
