package Pieces.ChanceCards;

import Players.Player;

public class YourBuildingLoanMatures extends ChanceCard {

    public YourBuildingLoanMatures () {
        this.description = "Your building loan matures – Collect $150";
    }

    public void action(Player currPlayer) {
        currPlayer.chargeBank(150);
    }
}
