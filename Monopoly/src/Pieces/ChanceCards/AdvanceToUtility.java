package Pieces.ChanceCards;

import Board.Utilities;
import Board.Space;
import Players.Player;

public class AdvanceToUtility extends ChanceCard {

    public AdvanceToUtility() {
        this.description = "Advance token to nearest Utility. If unowned, you may buy it from the Bank. If owned, throw"+
                " dice and pay owner a total ten times the amount thrown.";
    }

    public Space findClosestUtil(Space currSpace) {
        if(currSpace instanceof Utilities){
            return currSpace;
        }
        else {
            return findClosestUtil(currSpace.move());
        }
    }

    public void action(Player currPlayer) {
        currPlayer.setSpace(findClosestUtil(currPlayer.currSpace));
    }
}
