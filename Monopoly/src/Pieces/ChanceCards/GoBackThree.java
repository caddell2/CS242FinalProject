package Pieces.ChanceCards;

import Board.Space;
import Players.Player;

public class GoBackThree extends ChanceCard {

    public GoBackThree() {
        this.description = "Go Back 3 Spaces";
    }

    public void action(Player currPlayer) {
        Space toMoveTo = currPlayer.currSpace.previous().previous().previous();
        currPlayer.setSpace(toMoveTo);
    }

}
