package Pieces.ChanceCards;

import Board.Space;
import Players.Player;

public class AdvanceToIllinois extends ChanceCard {

    Space illinois;

    public AdvanceToIllinois(Space ill) {
        this.description = "Advance to Illinois Ave. - If you pass Go, collect $200";
        illinois = ill;
    }

    public void action(Player currPlayer) {
        currPlayer.setSpace(illinois);
    }

}
