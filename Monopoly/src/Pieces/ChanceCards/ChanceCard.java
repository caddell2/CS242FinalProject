package Pieces.ChanceCards;

import Players.Player;

public abstract class ChanceCard {

    String description;

    public String getDescrip() {
        return description;
    }

    public abstract void action(Player currPlayer);
}
