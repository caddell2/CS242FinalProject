package Pieces.ChanceCards;

import Board.Space;
import Players.Player;

public class TakeWalkOnBoardwalk extends ChanceCard{

    Space boardwalk;

    public TakeWalkOnBoardwalk(Space boardwalk) {
        this.description = "Take a walk on the Boardwalk – Advance token to Boardwalk";
        this.boardwalk = boardwalk;
    }

    public void action(Player currPlayer) {
        currPlayer.setSpace(boardwalk);
    }
}
