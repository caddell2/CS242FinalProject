package Pieces.ChanceCards;

import Board.Space;
import Players.Player;

public class AdvanceToStCharles extends ChanceCard{

    Space stCharles;

    public AdvanceToStCharles(Space stCharles) {
        this.description = "Advance to St. Charles Place – If you pass Go, collect $200";
        this.stCharles = stCharles;
    }

    public void action(Player currPlayer) {
        currPlayer.setSpace(stCharles);
    }

}
