package Pieces.ChanceCards;

import Players.Player;

public class MakeGeneralRepairs extends ChanceCard {

    public MakeGeneralRepairs() {
        this.description = "Make general repairs on all your property – For each house pay $25 – For each hotel $100";
    }

    public void action(Player currPlayer) {
        currPlayer.chargePerUnit(25, 100);
    }
}
