package Pieces.ChanceCards;

import Board.Space;
import Players.Player;

public class GoToJailChance extends ChanceCard{

    Space jail;

    public GoToJailChance(Space jail) {
        this.description = "Go to Jail – Go directly to Jail – Do not pass Go, do not collect $200";
        this.jail = jail;
    }

    public void action(Player currPlayer) {
        currPlayer.setSpace(jail);
    }
}
