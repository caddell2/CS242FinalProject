package Pieces.ChanceCards;

import Board.Space;
import Players.Player;

public class TakeTripToReading extends ChanceCard {

    Space readingRR;

    public TakeTripToReading(Space rRR) {
        this.description = "Take a trip to Reading Railroad – If you pass Go, collect $200";
        this.readingRR = rRR;
    }

    public void action(Player currPlayer) {
        currPlayer.setSpace(readingRR);
    }
}
