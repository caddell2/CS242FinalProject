package Pieces;

public class Dice {
    int value;

    public Dice() {
        value = 1;
    }

    public int roll() {
        value = (int)(Math.random() * 6) + 1;
        return value;
    }

    @Override
    public String toString() {
        if(value == 1)
            return "⚀";
        else if(value == 2)
            return "⚁";
        else if(value == 3)
            return "⚂";
        else if(value == 4)
            return "⚃";
        else if(value == 5)
            return "⚄";
        else
            return "⚅";
    }

    public int getValue() {
        return value;
    }
}
