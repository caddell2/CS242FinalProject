import Board.MonopolyBoard;
import Board.Property;
import Board.Space;
import GUI.BoardGUI;
import Pieces.Dice;
import Players.Player;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

import static java.lang.Integer.parseInt;
import static java.lang.System.exit;

public class Main {

    static MonopolyBoard board;
    static ArrayList<Player> players;
    static boolean gameOver;
    static Dice die1;
    static Dice die2;
    public static JFrame gui;
    public static int currPlayerNum;
    public static int numPlayers;

    public static void main(String[] args) throws FileNotFoundException, IOException{
        startGame();
        System.out.println(board.toString());
        movePrompt();
    }

    public static JPanel createButtonRow() {
        Player currPlayer = players.get(currPlayerNum-1);
        if(currPlayer.currSpace == board.jail){
            JPanel buttonPane = new JPanel(new GridLayout(1, 6));
            buttonPane.add(createRollButton());
            buttonPane.add(createCardButton());
            buttonPane.add(createPayButton());
            buttonPane.add(createHousesButton());
            buttonPane.add(createTradeButton());
            buttonPane.add(createHelpButton());
            return buttonPane;
        }
        else {
            JPanel buttonPane = new JPanel(new GridLayout(1, 4));
            buttonPane.add(createRollButton());
            buttonPane.add(createHousesButton());
            buttonPane.add(createTradeButton());
            buttonPane.add(createHelpButton());
            return buttonPane;
        }

    }

    public static JButton createRollButton() {
        JButton startGameButton = new JButton("Roll");
        startGameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)  {
                try {
                    move("roll");
                } catch (FileNotFoundException e1) {
                    board.log.logError(""+e1);
                }
            }
        });
        return startGameButton;
    }

    public static JButton createHousesButton() {
        return new JButton("Manage Property");
    }

    public static JButton createTradeButton() {
        return new JButton("Trade");
    }

    public static JButton createHelpButton() {
        return new JButton("?");
    }

    public static JButton createCardButton() {
        return new JButton("Get Out Card");
    }

    public static JButton createPayButton() {
        return new JButton("Buy Out");
    }


    public static void startGame() throws IOException{
        board = new MonopolyBoard();
        players = new ArrayList<>();
        String enteredNumPlayers = JOptionPane.showInputDialog(gui,"How many players do you want to have? (2-4)");
        numPlayers = parseInt(enteredNumPlayers);
        board.log.logInfo(""+numPlayers+" players to play in this game.");
        if(numPlayers < 2 || numPlayers > 4) {
            board.log.logError("Incorrect number of players! (Exiting)");
            exit(1);
        }
        for(int i = 1; i <= numPlayers; i++) {
            System.out.print("Enter name for player "+i+": ");
            String enteredName = JOptionPane.showInputDialog(gui,("Enter name for player "+i+": "));
            board.log.logInfo("Player "+i+" named: "+enteredName);
            players.add(new Player(enteredName, i, board.go, board));
            board.addPlayer(players.get(i-1));
        }
        gameOver = false;
        die1 = new Dice();
        die2 = new Dice();
        Image img = new ImageIcon("img/monopolyBoard.jpg").getImage();
        gui = new JFrame();
        gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gui.setSize((int)(img.getWidth(null)/2), (int)(img.getHeight(null)/2)+60);
        gui.add(BoardGUI.startBoardGUI(board), BorderLayout.CENTER);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        gui.setLocation(dim.width/2-gui.getSize().width/2, dim.height/2-gui.getSize().height/2);
        gui.setVisible(true);
        currPlayerNum = 1;
    }

    public static void movePrompt() {
        if (currPlayerNum > numPlayers) {
            currPlayerNum = 1;
        }
        board.levelUpProperties();
        gui.add(createButtonRow(), BorderLayout.SOUTH);
        gui.setVisible(true);
        board.log.logInfo(board.toString());
        Player currPlayer = players.get(currPlayerNum-1);
        if(currPlayer.currSpace == board.jail)
            JOptionPane.showMessageDialog(gui,"" + currPlayer.name + ", you are currently in jail, choose what you would like to do...");
        else
            JOptionPane.showMessageDialog(gui, ""+currPlayer.name+", choose what you would like to do...");
    }

    public static void move(String command) throws FileNotFoundException{
        System.out.println(board.toString());
        Player currPlayer = players.get(currPlayerNum-1);
        if(currPlayer.currSpace == board.jail) {
            if(command.equals("?")) {
                JOptionPane.showMessageDialog(gui,"\nroll: Roll the two dice to see if you get a double.\n" +
                        "card: Use a get out of jail free card (If done before first roll you can roll for a move)\n" +
                        "pay: Pay $50 and get out (Must be done after three rolls, but allows you to roll if you do it first)\n" +
                        "houses: Buy a house/hotel for a given space\n" +
                        "quit: Quit the game.\n" +
                        "?: Get help\n");
            }
            else if(command.equals("houses")) {
                String propertyString = ("Buy house/hotel for which property? You have the following (Re-type name to choose)\n");
                for(Space property : currPlayer.properties) {
                    if(property instanceof Property)
                        propertyString += (""+((Property) property).name+" (Development Cost: "+((Property) property).buildingCost+")\n");
                }
                propertyString += ("Choice: ");
                String choice = JOptionPane.showInputDialog(gui, propertyString);
                for(Space property : currPlayer.properties) {
                    if(property instanceof Property && ((Property) property).name.equals(choice))
                        board.buyHouse(currPlayer, property);
                }
            } else if(command.equals("trade")) {
                String prompt = "Which of the following players would you like to trade with?\n(Enter their name)\n";
                for(Player otherPlayer : players)
                    prompt += ""+otherPlayer.name+"\n";
                String nameOfPlayer = JOptionPane.showInputDialog(null, prompt);
                for(Player otherPlayer : players) {
                    if(otherPlayer.name.equals(nameOfPlayer)) {
                        currPlayer.offerTrade(otherPlayer);
                    }
                }
            }
            else if(command.equals("roll")) {
                die1.roll();
                die2.roll();
                System.out.println("The roll was: "+die1.toString()+die2.toString());
                if(die1.getValue() == die2.getValue()) {
                    currPlayer.setSpace(currPlayer.currSpace.next);
                    currPlayer.numJailTurns = 0;
                }
                else {
                    currPlayer.numJailTurns++;
                }
                currPlayerNum++;
            }
            else if(command.equals("card")) {
                if(currPlayer.hasCommunityGetOutofJailCard) {
                    currPlayer.hasCommunityGetOutofJailCard = false;
                    if(currPlayer.numJailTurns != 0) {
                        currPlayer.numJailTurns = 0;
                        currPlayerNum++;
                    }
                }
                else if(currPlayer.hasChanceGetOutofJailCard) {
                    currPlayer.hasChanceGetOutofJailCard = false;
                    if(currPlayer.numJailTurns != 0) {
                        currPlayer.numJailTurns = 0;
                        currPlayerNum++;
                    }

                }
                else {
                    JOptionPane.showMessageDialog(gui,"You can't do that, you don't have a Get Out of Jail Card");
                }
            }
            else if(command.equals("pay") || currPlayer.numJailTurns == 3) {
                currPlayer.giveMoneyToBank(50);
                currPlayer.setSpace(currPlayer.currSpace.next);
                currPlayer.numJailTurns = 0;
            }
            else if(command.equals("quit"))
                gameOver = true;
            else{
                board.log.logError("No known command entered! Attempting move again.");
            }
        }
        else {
            if (command.equals("?")) {
                JOptionPane.showMessageDialog(gui,"\nroll: Roll the two dice and move to the new space.\n" +
                        "houses: Buy a house/hotel for a given space" +
                        "quit: Quit the game.\n" +
                        "?: Get help\n");
            } else if (command.equals("roll")) {
                die1.roll();
                die2.roll();
                JOptionPane.showMessageDialog(gui,"The roll was: " + die1.toString() + die2.toString());
                board.log.logInfo("The roll was: " + die1.toString() + die2.toString());
                int spacesToMove = die1.getValue() + die2.getValue();
                while (spacesToMove > 0) {
                    if (spacesToMove != 1 && currPlayer.currSpace == board.go) {
                        currPlayer.chargeBank(200);
                    }
                    currPlayer.setSpace(currPlayer.currSpace.move());
                    spacesToMove--;
                }
                JOptionPane.showMessageDialog(gui,"You are now on " + currPlayer.currSpace.name);
                board.log.logInfo("You are now on " + currPlayer.currSpace.name);
                currPlayer.doSpace();
                if (die1.getValue() != die2.getValue())
                    currPlayerNum++;
            } else if(command.equals("trade")) {
                String prompt = "Which of the following players would you like to trade with?\n(Enter their name)\n";
                for(Player otherPlayer : players)
                    prompt += ""+otherPlayer.name+"\n";
                String nameOfPlayer = JOptionPane.showInputDialog(null, prompt);
                for(Player otherPlayer : players) {
                    if(otherPlayer.name.equals(nameOfPlayer)) {
                        currPlayer.offerTrade(otherPlayer);
                    }
                }
            } else if (command.equals("houses")) {
                String propertyString = ("Buy house/hotel for which property? You have the following (Re-type name to choose)\n");
                for(Space property : currPlayer.properties) {
                    if(property instanceof Property)
                        propertyString += (""+((Property) property).name+" (Development Cost: "+((Property) property).buildingCost+")\n");
                }
                propertyString += ("Choice: ");
                String choice = JOptionPane.showInputDialog(gui, propertyString);
                for(Space property : currPlayer.properties) {
                    if(property instanceof Property && ((Property) property).name.equals(choice))
                        board.buyHouse(currPlayer, property);
                }
            } else if (command.equals("quit")) {
                board.log.writeToFile("Game Log.txt");
                exit(1);
            }
            else {
                board.log.logError("No known command entered! Attempting move again.");
            }
        }
        movePrompt();
    }

}
