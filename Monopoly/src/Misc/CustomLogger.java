package Misc;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

public class CustomLogger {

    private ArrayList<String> log;

    public CustomLogger() {
        log = new ArrayList<>();
    }

    public void logError(String error) {
        String errMsg = "ERROR - "+error;
        System.out.println(errMsg);
        log.add(errMsg);
    }

    public void logWarning(String warning) {
        String warnMsg = "WARNING - "+warning;
        System.out.println(warnMsg);
        log.add(warnMsg);
    }

    public void logInfo(String info) {
        String infoMsg = "INFO - "+info;
        System.out.println(infoMsg);
        log.add(infoMsg);
    }

    public ArrayList getLog() {
        return log;
    }

    public void writeToFile(String fileName) throws FileNotFoundException {
        PrintWriter pw = new PrintWriter(new FileOutputStream(fileName));
        for (String logMsg : log)
            pw.println(logMsg);
        pw.close();
    }
}
