package Players;

import Board.*;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Player {

    public String name;
    public int numJailTurns;
    public int order;
    public int money;
    public int houseCount;
    public int hotelCount;
    public Space currSpace;
    public boolean hasChanceGetOutofJailCard;
    public boolean hasCommunityGetOutofJailCard;
    public ArrayList<Space> properties;
    MonopolyBoard currBoard;

    public Player(String name, int order, Space startSpace, MonopolyBoard currBoard) {
        this.name = name;
        this.numJailTurns = 0;
        this.order = order;
        this.money = 1500;
        this.currSpace = startSpace;
        this.hasChanceGetOutofJailCard = false;
        this.hasCommunityGetOutofJailCard = false;
        this.currBoard = currBoard;
        this.houseCount = 0;
        this.hotelCount = 0;
        this.properties = new ArrayList<>();
    }

    public Player(Player other) {
        this.name = other.name;
        this.numJailTurns = other.numJailTurns;
        this.order = other.order;
        this.money = other.money;
        this.currSpace = other.currSpace;
        this.hasChanceGetOutofJailCard = other.hasChanceGetOutofJailCard;
        this.hasCommunityGetOutofJailCard = other.hasCommunityGetOutofJailCard;
        this.currBoard = other.currBoard;
        this.houseCount = other.houseCount;
        this.hotelCount = other.hotelCount;
        this.properties = other.properties;

    }

    @Override
    public boolean equals(Object other) {
        if(other == null)
            return false;
        if(!(other instanceof Player))
            return false;
        Player oth = (Player) other;
        if(!this.name.equals(oth.name))
            return false;
        else if(this.numJailTurns != oth.numJailTurns)
            return false;
        else if(this.order != oth.order)
            return false;
        else if(this.money != oth.money)
            return false;
        else if(!(this.currSpace.equals(oth.currSpace)))
            return false;
        else if(this.hasChanceGetOutofJailCard != oth.hasChanceGetOutofJailCard)
            return false;
        else if(this.hasCommunityGetOutofJailCard != oth.hasCommunityGetOutofJailCard)
            return false;
        else if(this.houseCount != oth.houseCount)
            return false;
        else if(this.hotelCount != oth.hotelCount)
            return false;
        else
            return true;
    }

    public boolean wantsToBuy(Space space) {
        int cost = -1;
        if(space instanceof Property)
            cost = ((Property) space).cost;
        else if(space instanceof Railroad)
            cost = ((Railroad) space).cost;
        else if(space instanceof Utilities)
            cost = ((Utilities) space).cost;
        String input = JOptionPane.showInputDialog(null,"Do you want to buy this property for $"+cost+" (y/n): ");
        if(input.equals("y"))
            return true;
        else
            currBoard.auction(space);
        return false;
    }

    public void charge(int cost) {
        money = money - cost;
    }

    public void giveMoneyToBank(int amount) { currBoard.bank.takeMoney(amount, this);}

    public void getMoney(int amount) {
        money += amount;
    }

    public void chargeBank(int amount) { currBoard.bank.givePlayer(amount, this); }

    public void setSpace(Space space) {
        currSpace = space;
    }

    public void doSpace() {
        currSpace.action(this);
    }

    public void getChanceCard() {
        Space oldSpace = currSpace;
        currBoard.getChanceCard().action(this);
        if(currSpace != oldSpace) {
            doSpace();
        }
    }

    public void getCommunityChestCard() {
        Space oldSpace = currSpace;
        currBoard.getCommunityChestCard().action(this);
        if(currSpace != oldSpace) {
            doSpace();
        }
    }

    public void chargeAllOthers(int amount) {
        for (Player otherPlayer : currBoard.players) {
            otherPlayer.charge(amount);
            this.getMoney(amount);
        }
    }

    public void payAllOthers(int amount) {
        for (Player otherPlayer : currBoard.players) {
            otherPlayer.getMoney(amount);
            this.charge(amount);
        }
    }

    public void chargePerUnit(int houseAmount, int hotelAmount) {
        int amount = houseCount*houseAmount + hotelCount*hotelAmount;
        this.giveMoneyToBank(amount);
    }

    public void offerTrade(Player otherPlayer) {
        String moneyString = JOptionPane.showInputDialog(null, "What amount of cash would you like to offer?");
        int moneyOffer = Integer.parseInt(moneyString);
        String propertyPrompt = ("Which of the following properties would you like to offer?\n(Seperate list with commas, and no spaces)\n");
        for(Space property : properties) {
            propertyPrompt += ""+property.name+"\n";
        }
        String propertyOffer = JOptionPane.showInputDialog(null, propertyPrompt);
        moneyString = JOptionPane.showInputDialog(null, "What amount of cash would you like to ask for?");
        int moneyAsk = Integer.parseInt(moneyString);
        propertyPrompt = ("Which of the following properties would you like to offer?\n(Seperate list with commas, and no spaces)\n");
        for(Space property : otherPlayer.properties) {
            propertyPrompt += ""+property.name+"\n";
        }
        String propertyAsk = JOptionPane.showInputDialog(null, propertyPrompt);

        boolean accepted = otherPlayer.receiveTrade(this, moneyOffer, propertyOffer, moneyAsk, propertyAsk);

        if(accepted) {
            giveTradeMoney(moneyOffer, otherPlayer);
            String[] propertiesToGive = propertyOffer.split(",");
            for(String propName : propertiesToGive) {
                for(Space property : properties) {
                    if(property.name.equals(propName)) {
                        properties.remove(property);
                        otherPlayer.properties.add(property);
                    }
                }
            }
            otherPlayer.giveTradeMoney(moneyAsk, this);
            String[] propertiesToGet = propertyAsk.split(",");
            for(String propName : propertiesToGet) {
                for(Space property : otherPlayer.properties) {
                    if(property.name.equals(propName)) {
                        otherPlayer.properties.remove(property);
                        properties.add(property);
                    }
                }
            }
        }

    }

    public boolean receiveTrade(Player initPlayer, int moneyOffered, String propertyOffered, int moneyAsked, String propertyAsked) {
        String prompt = ""+name+", would you accept the following trade from "+initPlayer.name+"? (y/n)\n";
        if(moneyOffered > 0)
            prompt += "$"+moneyOffered+"\n";
        if(propertyOffered.length() > 0)
            prompt += "These Properties: "+propertyOffered+"\n";
        prompt += "\n In exchange for...\n\n";
        if(moneyAsked > 0)
            prompt += "$"+moneyOffered+"\n";
        if(propertyAsked.length() > 0)
            prompt += "These Properties: "+propertyOffered+"\n";
        String response = JOptionPane.showInputDialog(null, prompt);
        return (response.equals("y")) ? true : false;
    }

    public void giveTradeMoney(int amount, Player otherPlayer) {
        this.chargeBank(amount);
        otherPlayer.getMoney(amount);
    }
}
