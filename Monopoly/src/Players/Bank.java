package Players;

public class Bank {

    int amount;

    public Bank() {
        amount = 15140;
    }

    public void givePlayer(int charge, Player player) {
        amount -= charge;
        player.getMoney(amount);
    }

    public void takeMoney(int amount, Player player) {
        this.amount += amount;
        player.charge(amount);
    }
}
