package Board;

import Players.Player;

/**
 * @author Matt Caddell
 * A class designed to represent the Free Parking Space
 */
public class FreeParking extends Space{


    /**
     * The constuctor
     */
    public FreeParking(Space next, Space prev, String name) {
        this.next = next;
        this.prev = prev;
        this.name = name;
    }

    /**
     * Does nothing
     */
    public void action(Player currPlayer)  {
        return;
    }
}
