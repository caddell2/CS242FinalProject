package Board;

import Players.Player;

/**
 * @author Matt Caddell
 * A class designed to represent the jail space
 */
public class Jail extends Space{

    /**
     * The constructor
     * @param next - The next space
     * @param prev - The previous space
     */
    public Jail(Space next, Space prev, String name) {
        this.next = next;
        this.prev = prev;
        this.name = name;
    }


    /**
     * Counts the turn as spent in jail
     * @param currPlayer - The current player
     */
    public void action(Player currPlayer) {
        return;
    }
}
