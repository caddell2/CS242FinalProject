package Board;

import Players.Player;

import java.awt.*;
import java.util.Arrays;

public class Property extends Space {
    public int cost;
    public int buildingCost;
    public int level;
    int[] rent;
    public Color color;
    Player owner;

    public Property(Space next, Space prev, String name, int cost, int buildingCost, int[] rent, Color color) {
        this.next = next;
        this.prev = prev;
        this.name = name;
        this.cost = cost;
        this.buildingCost = buildingCost;
        this.rent = Arrays.copyOf(rent, rent.length);
        this.color = color;
        this.level = 0;
        this.owner = null;
    }

    public void action(Player currPlayer) {
        if(owner == null){
            if(currPlayer.wantsToBuy(this)) {
                currPlayer.charge(cost);
                currPlayer.properties.add(this);
                owner = currPlayer;
            }
            else { return; } // Calls for the auction in wantsToBuy() since it allows easier access to the board and all players
        }
        else {
            currPlayer.charge(rent[level]);
            owner.getMoney(rent[level]);
        }
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
