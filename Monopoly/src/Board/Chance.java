package Board;

import Players.Player;

/**
 * @author Matt Caddell
 * A class designed to represent the spaces on the board that make the player draw a Chance card
 */
public class Chance extends Space{

    /**
     * The constructor
     */
    public Chance(Space next, Space prev, String name) {
        this.next = next;
        this.prev = prev;
        this.name = name;
    }


    /**
     * Makes the player draw a Chance card
     */
    public void action(Player currPlayer) {
        currPlayer.getChanceCard();
    }
}
