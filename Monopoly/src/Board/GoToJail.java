package Board;

import Players.Player;

/**
 * @author Matt Caddell
 * A class designed to represent the space that sends the player straight to jail
 */
public class GoToJail extends Space{

    //A variable to hold the Jail space to be called later
    Space jail;


    /**
     * The Constructor
     * @param next - The next space
     * @param prev - The previous space
     * @param jail - The jail space
     */
    public GoToJail(Space next, Space prev, String name, Space jail) {
        this.next = next;
        this.prev = prev;
        this.name = name;
        this.jail = jail;
    }

    /**
     * Sends the player to jail
     * @param currPlayer - The current player
     */
    public void action(Player currPlayer) {
        currPlayer.setSpace(jail);
    }

}
