package Board;

import Pieces.Dice;
import Players.Player;

public class Utilities extends Space {

    int level;
    public int cost;
    Player owner;

    public Utilities(Space next, Space prev, String name) {
        this.next = next;
        this.prev = prev;
        this.name = name;
        this.level = 0;
        this.cost = 150;
        this.owner = null;
    }

    public void action(Player currPlayer) {
        if(owner == null){
            if(currPlayer.wantsToBuy(this)) {
                currPlayer.charge(cost);
                owner = currPlayer;
            }
            else { return; }
        }
        else {
            Dice utilityDie = new Dice();
            if(level == 0) {
                int amount = utilityDie.roll()*4;
                currPlayer.charge(amount);
                owner.getMoney(amount);
            }
            else {
                int amount = utilityDie.roll()*10;
                currPlayer.charge(amount);
                owner.getMoney(amount);
            }

        }
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
