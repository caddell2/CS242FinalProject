package Board;

import Players.Player;

/**
 * @author Matt Caddell
 * A class designed to represent the spaces on the board that make the player draw a Community Chest card
 */
public class CommunityChest extends Space{

    /**
     * The constuctor
     */
    public CommunityChest(Space next, Space prev, String name) {
        this.next = next;
        this.prev = prev;
        this.name = name;
    }


    /**
     * Makes the player draw a community chest card
     */
    public void action(Player currPlayer) {
        currPlayer.getCommunityChestCard();
    }
}
