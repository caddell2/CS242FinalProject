package Board;

import Players.Player;

/**
 * @author Matt Caddell
 * A class designed to represent the go/start space in Monopoly
 */
public class Go extends Space{

    /**
     * The constructor
     */
    public Go(Space next, Space prev, String name) {
        this.next = next;
        this.prev = prev;
        this.name = name;
    }

    /**
     * Gives the player $200 (Should also do it if just passed over. @TODO Implement that
     */
    public void action(Player currPlayer) {
        currPlayer.chargeBank(200);
    }
}

