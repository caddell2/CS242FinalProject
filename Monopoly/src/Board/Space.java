package Board;

import Players.Player;

abstract public class Space {
    public Space next;
    public String name;
    Space prev;

    public Space move() {
        return next;
    }

    public Space previous() {
        return prev;
    }

    public Space last() {
        return lastRecur(this);
    }

    private static Space lastRecur(Space curr) {
        if(curr.next == null)
            return curr;
        else
            return lastRecur(curr.next);
    }

    @Override
    public boolean equals(Object oth) {
        if(oth == null)
            return false;
        if(!(oth instanceof Space))
            return false;
        Space other = (Space) oth;
        if(next != other.next)
            return false;
        else if(prev != other.prev)
            return false;
        else
            return true;
    }

    public abstract void action(Player currPlayer);
}
