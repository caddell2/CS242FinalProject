package Board;

import Players.Player;

public class Tax extends Space{
    int cost;

    public Tax(Space next, Space prev, String name, int cost) {
        this.next = next;
        this.prev = prev;
        this.name = name;
        this.cost = cost;
    }

    public void action(Player currPlayer) {
        currPlayer.giveMoneyToBank(cost);
    }

}
