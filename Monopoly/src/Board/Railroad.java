package Board;

import Players.Player;


public class Railroad extends Space{
    public int cost;
    public int level;
    Player owner;

    public Railroad(Space next, Space prev, String name) {
        this.next = next;
        this.prev = prev;
        this.name = name;
        this.cost = 200;
        this.level = 0;
        this.owner = null;
    }

    public void action(Player currPlayer) {
        if(owner == null){
            if(currPlayer.wantsToBuy(this)) {
                currPlayer.charge(cost);
                owner = currPlayer;
            }
            else { return; }
        }
        else {
            currPlayer.charge((int)(25* Math.pow(2, level)));
            owner.getMoney((int)(25* Math.pow(2, level)));
        }
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
