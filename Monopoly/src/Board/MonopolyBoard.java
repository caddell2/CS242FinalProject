package Board;

import Misc.CustomLogger;
import Pieces.ChanceCards.*;
import Pieces.CommunityChestCards.*;
import Pieces.Dice;
import Players.Bank;
import Players.Player;

import javax.swing.*;
import java.awt.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class MonopolyBoard {

    // The bank
    public Bank bank;

    // The first space on the board
    public Space go;

    // The jail space on the board, saved for easy access
    public Space jail;

    // An arraylist of the players in the game (Currently limited to 2-4)
    public ArrayList<Player> players;

    // The chance deck
    public ArrayList<ChanceCard> ChanceDeck;

    // The community chest deck
    public ArrayList<CommunityChestCard> CommunityChestDeck;

    // A custom log that I built
    public CustomLogger log;

    /**
     * The default (and only) constructor. Calls a function that builds the board.
     */
    public MonopolyBoard() {
        initializeBoard();
    }

    /**
     * Adds a player to the board
     * @param newPlayer - The player to add to the board.
     */
    public void addPlayer(Player newPlayer) {
        players.add(newPlayer);
    }

    /**
     * Gets a card from the chance deck
     * @return The card that is drawn from the deck
     */
    public ChanceCard getChanceCard() {
        ChanceCard tmp = ChanceDeck.remove(ChanceDeck.size()-1);
        log.logInfo("Drawing Chance Card: "+tmp.getDescrip());
        return tmp;
    }

    /**
     * Gets a card from the community chest deck
     * @return The card that is drawn from the deck
     */
    public CommunityChestCard getCommunityChestCard() {
        CommunityChestCard tmp = CommunityChestDeck.remove(CommunityChestDeck.size()-1);
        log.logInfo("Drawing Community Chest Card: "+tmp.getDescrip());
        return tmp;
    }

    /**
     * Initializes the Chance deck, takes all the addresses of spaces for cards that send you straight to a space
     * @param go - The go space address
     * @param ill - The space address for Illinois Ave
     * @param stCharles - The space address for St. Charles
     * @param jail - The space address for jail
     * @param rRR - The space address for Reading Rail Road
     * @param boardwalk - The space address for the Boardwalk
     */
    public void initializeChanceDeck(Space go, Space ill, Space stCharles, Space jail, Space rRR, Space boardwalk) {
        ChanceDeck = new ArrayList<>();
        ChanceDeck.add(new AdvanceToGoChance(go));
        ChanceDeck.add(new AdvanceToIllinois(ill));
        ChanceDeck.add(new AdvanceToRailRoad());
        ChanceDeck.add(new AdvanceToRailRoad());
        ChanceDeck.add(new AdvanceToStCharles(stCharles));
        ChanceDeck.add(new AdvanceToUtility());
        ChanceDeck.add(new BankPaysYou());
        ChanceDeck.add(new GetOutOfJailChance());
        ChanceDeck.add(new GoBackThree());
        ChanceDeck.add(new GoToJailChance(jail));
        ChanceDeck.add(new MakeGeneralRepairs());
        ChanceDeck.add(new PayPoorTax());
        ChanceDeck.add(new TakeTripToReading(rRR));
        ChanceDeck.add(new TakeWalkOnBoardwalk(boardwalk));
        ChanceDeck.add(new YouAreChairman());
        ChanceDeck.add(new YourBuildingLoanMatures());
        Collections.shuffle(ChanceDeck);
    }

    /**
     * Initiailizes the community chest deck
     * @param go - The go space address
     * @param jail - The address of the jail space
     */
    public void initializeCommunityChestDeck(Space go, Space jail) {
        CommunityChestDeck = new ArrayList<>();
        CommunityChestDeck.add(new AdvanceToGoCC(go));
        CommunityChestDeck.add(new BankError());
        CommunityChestDeck.add(new DoctorFees());
        CommunityChestDeck.add(new FromStockSale());
        CommunityChestDeck.add(new GetOutOfJailCC());
        CommunityChestDeck.add(new GoToJailCC(jail));
        CommunityChestDeck.add(new GrandOpera());
        CommunityChestDeck.add(new HolidayFund());
        CommunityChestDeck.add(new IncomeTaxRefund());
        CommunityChestDeck.add(new LifeInsurance());
        CommunityChestDeck.add(new PayHospital());
        CommunityChestDeck.add(new PaySchool());
        CommunityChestDeck.add(new ReceiveConsultantFee());
        CommunityChestDeck.add(new YouGetStreetRepairs());
        CommunityChestDeck.add(new YouReceivedInheitance());
        CommunityChestDeck.add(new YouWonBeauty());
        Collections.shuffle(CommunityChestDeck);
    }

    /**
     * Initializes the board.
     * One unholy mess of a function.
     * @TODO: Cleanup... if possible?
     */
    public void initializeBoard() {
        log = new CustomLogger();
        log.logInfo("Creating Board");
        go = new Go(null, null, "Go");
        go.last().next = new Property(null, go.last(), "Mediterranean Avenue", 60, 50, (new int[]{2,4,10,30,90,160,250}), Color.white);
        go.last().next = new CommunityChest(null, go.last(), "Community Chest");
        go.last().next = new Property(null, go.last(), "Baltic Avenue", 60, 50, (new int[]{4,8,20,60,180,320,450}), Color.white);
        go.last().next = new Tax(null, go.last(), "Income Tax", 200);
        Space rRR = new Railroad(null, go.last(), "Reading Railroad");
        go.last().next = rRR;
        go.last().next = new Property(null, go.last(), "Oriental Avenue", 100, 50, (new int[]{6,12,30,90,270,400,550}), Color.cyan);
        go.last().next = new Chance(null, go.last(), "Chance");
        go.last().next = new Property(null, go.last(), "Vermont Avenue", 100, 50, (new int[]{6,12,30,90,270,400,550}), Color.cyan);
        go.last().next = new Property(null, go.last(), "Connecticut Avenue", 120, 60, (new int[]{8,16,40,100,300,450,600}), Color.cyan);
        go.last().next = new PassingJail(null, go.last(), "Passing Jail");
        jail = new Jail(go.last(), null, "Jail");
        Space stCharles = new Property(null, go.last(), "St.Charles Place", 140, 100, (new int[]{10, 20, 50, 150, 450, 625}), Color.magenta);
        go.last().next = stCharles;
        go.last().next = new Utilities(null, go.last(), "Electric Company");
        go.last().next = new Property(null, go.last(), "States Avenue", 140, 100, (new int[]{10, 20, 50, 150, 450, 625}), Color.magenta);
        go.last().next = new Property(null, go.last(), "Virginia Avenue", 160, 100, (new int[]{12,24,60,180,500,700,900}), Color.magenta);
        go.last().next = new Railroad(null, go.last(), "Pennsylvania Railroad");
        go.last().next = new Property(null, go.last(), "St.James Place", 180, 100, (new int[]{14, 28, 70, 200, 550, 750, 950}), Color.orange);
        go.last().next = new CommunityChest(null, go.last(), "Community Chest");
        go.last().next = new Property(null, go.last(), "Tennessee Avenue", 180, 100, (new int[]{14, 28, 70, 200, 550, 750, 950}), Color.orange);
        go.last().next = new Property(null, go.last(), "New York Avenue", 200, 100, (new int[]{16, 32, 80, 220, 600, 800, 1000}), Color.orange);
        go.last().next = new FreeParking(null, go.last(), "Free Parking");
        go.last().next = new Property(null, go.last(), "Kentucky Avenue", 220, 150, (new int[]{18,36,90,250,700,875,1050}), Color.red);
        go.last().next = new Chance(null, go.last(), "Chance");
        go.last().next = new Property(null, go.last(), "Indiana Avenue", 220, 150, (new int[]{18,36,90,250,700,875,1050}), Color.red);
        Space ill = new Property(null, go.last(), "Illinois Avenue", 240, 150, (new int[]{20,40,100,300,750,925,1100}), Color.red);
        go.last().next = ill;
        go.last().next = new Railroad(null, go.last(), "B. & O. Railroad");
        go.last().next = new Property(null, go.last(), "Atlantic Avenue", 260, 150, (new int[]{22,44,110,330,800,975,1150}), Color.yellow);
        go.last().next = new Property(null, go.last(), "Ventnor Avenue", 260, 150, (new int[]{22,44,110,330,800,975,1150}), Color.yellow);
        go.last().next = new Utilities(null, go.last(), "Water Works");
        go.last().next = new Property(null, go.last(), "Marvin Gardens", 280, 150, (new int[]{24,48,120,360,850,1025,1200}), Color.yellow);
        go.last().next = new GoToJail(null, go.last(), "Go To Jail", jail);
        go.last().next = new Property(null, go.last(), "Pacific Avenue", 300, 200, (new int[]{26,52,130,390,900,1100,1275}), Color.green);
        go.last().next = new Property(null, go.last(), "North Carolina Avenue", 300, 200, (new int[]{26,52,130,390,900,1100,1275}), Color.green);
        go.last().next = new CommunityChest(null, go.last(), "Community Chest");
        go.last().next = new Property(null, go.last(), "Pennsylvania Avenue", 320, 200, (new int[]{28,56,150,450,1000,1200,1400}), Color.green);
        go.last().next = new Railroad(null, go.last(), "Short Line");
        go.last().next = new Chance(null, go.last(), "Chance");
        go.last().next = new Property(null, go.last(), "Park Place", 350, 200, (new int[]{35,70,175,500,1100,1300,1500}), Color.blue);
        go.last().next = new Tax(null, go.last(), "Luxury Tax", 100);
        Space boardwalk = new Property(null, go.last(), "Boardwalk", 400, 200, (new int[]{50,100,200,600,1400,1700,2000}), Color.blue);
        go.last().next = boardwalk;
        boardwalk.next = go;
        go.prev = boardwalk;

        log.logInfo("Creating Decks");
        initializeChanceDeck(go, ill, stCharles, jail, rRR, boardwalk);
        initializeCommunityChestDeck(go, jail);

        log.logInfo("Creating Bank and Player List");
        this.bank = new Bank();
        this.players = new ArrayList<>();

        log.logInfo("The Board:");
        log.logInfo(this.toString());
    }

    /**
     * Overriding of the default toString so we can print the condition of the board for use in logging,
     * and low level GUI interaction, and debugging.
     * @return The string representation of the board, including the locations of players, the amount
     *         cards left in the decks, and the amount of money players have.
     */
    @Override
    public String toString() {
        ArrayList<String> boardArrList = new ArrayList<>();
        boardArrList.add("--------------------------------------------------------\n");
        boardArrList.add("|    |    |    |    |    |    |    |    |    |    |    |\n");
        boardArrList.add("|    |    |    |    |    |    |    |    |    |    |    |\n");
        boardArrList.add("--------------------------------------------------------\n");
        boardArrList.add("|    |                                            |    |\n");
        boardArrList.add("|    |                                            |    |\n");
        boardArrList.add("------                                            ------\n");
        boardArrList.add("|    |    CC: xx/16                               |    |\n");
        boardArrList.add("|    |                                            |    |\n");
        boardArrList.add("------                                            ------\n");
        boardArrList.add("|    |                                            |    |\n");
        boardArrList.add("|    |                                            |    |\n");
        boardArrList.add("------                                            ------\n");
        boardArrList.add("|    |                                            |    |\n");
        boardArrList.add("|    |                                            |    |\n");
        boardArrList.add("------                                            ------\n");
        boardArrList.add("|    |                   Monopoly                 |    |\n");
        boardArrList.add("|    |                   By: Matt                 |    |\n");
        boardArrList.add("------                                            ------\n");
        boardArrList.add("|    |                                            |    |\n");
        boardArrList.add("|    |                                            |    |\n");
        boardArrList.add("------                                            ------\n");
        boardArrList.add("|    |                                            |    |\n");
        boardArrList.add("|    |                                            |    |\n");
        boardArrList.add("------                                            ------\n");
        boardArrList.add("|    |                                            |    |\n");
        boardArrList.add("|    |                             Chance: xx/16  |    |\n");
        boardArrList.add("-----------                                       ------\n");
        boardArrList.add("|    |    |                                       |    |\n");
        boardArrList.add("|    |    |                                       |    |\n");
        boardArrList.add("--------------------------------------------------------\n");
        boardArrList.add("|    |    |    |    |    |    |    |    |    |    |    |\n");
        boardArrList.add("|    |    |    |    |    |    |    |    |    |    |    |\n");
        boardArrList.add("--------------------------------------------------------\n");

        String ccCount = ""+(CommunityChestDeck.size());
        if(CommunityChestDeck.size() < 10)
            ccCount = "0"+ccCount;
        boardArrList.set(7, boardArrList.get(7).substring(0,14)+(ccCount)+boardArrList.get(7).substring(16));

        String chanceCount = ""+(CommunityChestDeck.size());
        if(CommunityChestDeck.size() < 10)
            chanceCount = "0"+chanceCount;
        boardArrList.set(26, boardArrList.get(26).substring(0,43)+(chanceCount)+boardArrList.get(26).substring(45));

        for(Player currPlayer : players) {
            int playerNum = currPlayer.order;
            int drawX = (playerNum - 1) % 2;
            int drawY = (playerNum > 2) ? 1 : 0;
            Space currSpace = currPlayer.currSpace;
            int count = 0;
            Space tmp = go;
            if(!currSpace.equals(go) && !currSpace.equals(jail)) {
                tmp = tmp.next;
                count++;
                while(!tmp.equals(go)) {
                    if(tmp.equals(currSpace)) {
                        break;
                    }
                    count++;
                    tmp = tmp.next;
                }
                if(tmp.equals(go))
                    log.logError("Unable to find space for player!");
            }
            if(currSpace.equals(jail)) {
                boardArrList.set((28+drawY), boardArrList.get((28+drawY)).substring(0,(7+drawX))+(playerNum)+boardArrList.get((28+drawY)).substring((8+drawX)));
            } else {
                int x = -1;
                int y = -1;
                if(count < 11) {
                    x = 11 - count;
                    y = 1;
                }
                else if(count >= 11 && count < 21) {
                    x = 1;
                    y = 1 + (count - 10);
                }
                else if(count >= 21 && count < 31) {
                    x = 1 + (count - 20);
                    y = 11;
                }
                else {
                    x = 11;
                    y = 11 - (count-30);
                }
                int row = ((11 - y) * 3) + 1;
                int col = (( x - 1) * 5) + 2;
                boardArrList.set((row+drawY), boardArrList.get((row+drawY)).substring(0,(col+drawX))+(playerNum)+boardArrList.get((row+drawY)).substring((col+1+drawX)));
            }
        }

        boardArrList.add("\n");
        for(Player currPlayer : players) {
            boardArrList.add(""+currPlayer.name+": "+currPlayer.money+"\n");
        }

        String tmp = "";
        for(String str : boardArrList)
            tmp = tmp + str;
        return tmp;
    }

    /**
     * Auctions off a particular property
     * @param space - The space ot be auctioned
     */
    public void auction(Space space) {
        Scanner reader = new Scanner(System.in);
        int currAmount = 0;
        Player lastBidder = null;
        Player currBidder = players.get(0);
        int next = 0;
        while(!lastBidder.equals(currBidder)) {
            next = (next+1)%(players.size());

            String input = JOptionPane.showInputDialog(null,"The current bid is $"+currAmount+"\nDo you wish to bid more? (y/n): ");
            if(input.equals("y")) {
                String amountString = JOptionPane.showInputDialog("How much would you like to bid: ");
                int amount = Integer.parseInt(amountString);
                if(amount < currAmount)
                    log.logError("Incorrect bidding amount!");
                else {
                    currAmount = amount;
                    lastBidder = currBidder;
                }

            }
            currBidder = players.get(next);
        }
        lastBidder.giveMoneyToBank(currAmount);
        lastBidder.properties.add(space);
    }

    public void levelUpProperties() {
        for(Player currPlayer : players) {
            for(Space property : currPlayer.properties) {
                if(property instanceof Property) {
                    Color currColor = ((Property) property).color;
                    int colorCount = 1;
                    for(Space tmp : currPlayer.properties) {
                        if(tmp instanceof Property && ((Property) tmp).color == currColor && !tmp.equals(property)) {
                            colorCount++;
                        }
                    }
                    if(currColor == Color.white || currColor == Color.blue) {
                        if(colorCount == 2 && ((Property) property).level == 0)
                            ((Property) property).setLevel(1);
                        else if(colorCount != 2 && ((Property) property).level == 1)
                            ((Property) property).setLevel(0);
                    }
                    else {
                        if(colorCount == 3 && ((Property) property).level == 0)
                            ((Property) property).setLevel(1);
                        else if(colorCount != 3 && ((Property) property).level == 1)
                            ((Property) property).setLevel(0);
                    }
                }
                else if(property instanceof Railroad) {
                    int rrCount = 0;
                    for(Space tmp : currPlayer.properties) {
                        if(tmp instanceof Railroad && !tmp.equals(property)) {
                            rrCount++;
                        }
                    }
                    ((Railroad) property).setLevel(rrCount);
                }
                else if(property instanceof Utilities) {
                    int utilCount = 0;
                    for(Space tmp : currPlayer.properties) {
                        if(tmp instanceof Utilities && !tmp.equals(property)) {
                            utilCount++;
                        }
                    }
                    ((Utilities) property).setLevel(utilCount);
                }
            }
        }
    }

    public void buyHouse(Player currPlayer, Space space) {
        if(space instanceof Property) {
            if(((Property) space).level == 0)
                log.logWarning("Unable to buy houses unless you have a monopoly!");
            else if(((Property) space).level == 6)
                log.logWarning("Unable to buy more, you have a hotel");
            else {
                currPlayer.giveMoneyToBank(((Property) space).buildingCost);
                ((Property) space).setLevel(((Property) space).level + 1);
            }
        }
        else {
            log.logWarning("Not a property!");
        }
    }
}
