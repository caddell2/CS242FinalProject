package GUI;

import Board.MonopolyBoard;
import Board.Space;
import Players.Player;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class BoardGUI {
    public static JPanel startBoardGUI(MonopolyBoard board) throws IOException{

        JPanel f = new JPanel()
        {
            public void paintComponent(Graphics g)
            {
                Image img = new ImageIcon("img/monopolyBoard.jpg").getImage();
                Image[] playerImages = {new ImageIcon("img/TopHatSprite.png").getImage(),
                        new ImageIcon("img/IronSprite.png").getImage(),
                        new ImageIcon("img/ThimbleSprite.png").getImage(),
                        new ImageIcon("img/DogSprite.png").getImage()};
                Dimension size = new Dimension((int)(img.getWidth(null)/2), (int)(img.getHeight(null)/2));
                setPreferredSize(size);
                setMinimumSize(size);
                setMaximumSize(size);
                setSize(size);
                setLayout(null);
                g.drawImage(img, 0, 0, getWidth(), getHeight(), this);
                for(Player currPlayer : board.players) {
                    int playerNum = currPlayer.order;
                    int drawX = (playerNum - 1) % 2;
                    int drawY = (playerNum > 2) ? 1 : 0;
                    Space currSpace = currPlayer.currSpace;
                    int count = 0;
                    Space tmp = board.go;
                    if(!currSpace.equals(board.go) && !currSpace.equals(board.jail)) {
                        tmp = tmp.next;
                        count++;
                        while(!tmp.equals(board.go)) {
                            if(tmp.equals(currSpace)) {
                                break;
                            }
                            count++;
                            tmp = tmp.next;
                        }
                        if(tmp.equals(board.go))
                            board.log.logError("Unable to find space for player!");
                    }
                    if(currSpace.equals(board.jail)) {
                        g.drawImage(playerImages[currPlayer.order], 55, size.height-55, playerImages[currPlayer.order].getWidth(null)*2, playerImages[currPlayer.order].getHeight(null)*2,this);
                    } else {
                        int x = -1;
                        int y = -1;
                        if(count < 11) {
                            x = 11 - count;
                            y = 1;
                        }
                        else if(count >= 11 && count < 21) {
                            x = 1;
                            y = 1 + (count - 10);
                        }
                        else if(count >= 21 && count < 31) {
                            x = 1 + (count - 20);
                            y = 11;
                        }
                        else {
                            x = 11;
                            y = 11 - (count-30);
                        }
                        int row = ((11 - y) * 3) + 1;
                        int col = (( x - 1) * 5) + 2;
                        g.drawImage(playerImages[currPlayer.order], size.height-55, size.height-55, playerImages[currPlayer.order].getWidth(null)*2, playerImages[currPlayer.order].getHeight(null)*2,this);
                    }
                }
            }
        };

        return f;
    }
}
